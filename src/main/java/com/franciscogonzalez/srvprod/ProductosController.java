package com.franciscogonzalez.srvprod;

import com.franciscogonzalez.srvprod.productos.ProductoModel;
import com.franciscogonzalez.srvprod.productos.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductosController {
    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos() {
        return productoService.findAll();
    }

    @PostMapping("/productos")
    public ProductoModel postProducto(@RequestBody ProductoModel prod) {
        return productoService.save(prod);
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel prod) {
        productoService.save(prod);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel prod) {
        return productoService.delete(prod);
    }
}
