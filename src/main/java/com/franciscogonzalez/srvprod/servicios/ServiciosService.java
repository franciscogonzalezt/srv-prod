package com.franciscogonzalez.srvprod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    static MongoCollection<Document> servicios;
    private static MongoCollection<Document> getServiciosCollection() {
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();

        MongoClient mongo = MongoClients.create(settings);
        MongoDatabase database = mongo.getDatabase("dbprod");

        return database.getCollection("servicios");
    }

    public static void insert(String servicio) throws Exception {
        servicios = getServiciosCollection();
        Document doc = Document.parse(servicio);
        List<Document> lst = doc.getList("servicios", Document.class);
        if (lst == null) {
            servicios.insertOne(doc);
        } else {
            servicios.insertMany(lst);
        }
    }

    public static List<Document> getAll() {
        servicios = getServiciosCollection();
        List list = new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }
    public static List<Document> getFiltrados(String filtro) {
        servicios = getServiciosCollection();
        List list = new ArrayList();
        Document doc = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(doc);
        Iterator it = iterDoc.iterator();
        while (it.hasNext()) {
            list.add(it.next());
        }
        return list;
    }

    public static void update(String filtro, String updates) {
        servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document doc = Document.parse(updates);
        servicios.updateOne(docFiltro, doc);
    }
}
