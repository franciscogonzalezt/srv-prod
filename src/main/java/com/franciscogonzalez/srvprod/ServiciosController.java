package com.franciscogonzalez.srvprod;

import com.franciscogonzalez.srvprod.servicios.ServiciosService;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ServiciosController {

    @GetMapping("/servicios")
    public List getServicios(@RequestBody String filtro) {
        return ServiciosService.getFiltrados(filtro);
    }

    @PostMapping("/servicios")
    public String setServicio(@RequestBody String newServicio){
        try {
            ServiciosService.insert(newServicio);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }

    @PutMapping("/servicios")
    public String updateServicios(@RequestBody String data) {
        try {
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ServiciosService.update(filtro, updates);
            return "OK";
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
